<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mutualiste extends Model
{
    use HasFactory;
    protected $fillable = ['Nom', 'Prenoms', 'Date_de_Naissance', 'Grade', 'Emploi', 'Telephone', 'Email', 'Profil', 'Statut_Compte','Matricule','Nom_Utilisateur'];
    //protected $fillable = ['Nom'];
}

