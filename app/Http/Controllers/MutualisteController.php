<?php

namespace App\Http\Controllers;

use App\Http\Resources\Mutualiste as ResourcesMutualiste;
use App\Models\Mutualiste;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class MutualisteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ResourcesMutualiste::collection(Mutualiste::orderByDesc('created_at')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Mutualiste::create($request->all()));
        return response()->json([
            'succes' => 'mutualiste creer avec succes'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mutualiste  $mutualiste
     * @return \Illuminate\Http\Response
     */
    public function show(Mutualiste $mutualiste)
    {
        return new ResourcesMutualiste($mutualiste);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mutualiste  $mutualiste
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mutualiste $mutualiste)
    {
        if ($mutualiste->update($request->all())){
            return response()->json([
              'success' => 'Mutualiste modifier avec succes'
            ],200);

             }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mutualiste  $mutualiste
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mutualiste $mutualiste)
    {
        $mutualiste->delete();
    }

    public function getMutualist($matricule)
    {

    }
}
