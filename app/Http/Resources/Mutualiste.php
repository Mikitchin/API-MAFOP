<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Mutualiste extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {


        return[
            'Nom' => $this->Nom,
            'Prenoms' => $this->Prenoms,
            'Date_de_Naissance' => $this->Date_de_Naissance,
            'Numéro_de_CNI' => $this->Numéro_de_CNI,
            'Nom_du_père' => $this->Nom_du_père,
            'Nom_de_la_Mère' => $this->Nom_de_la_Mère,
            'Emploie' => $this->Emploie,
            'Grade' => $this->Grade,
            'Date_de_première_prise_de_service' => $this->Date_de_première_prise_de_service,
            'Date_de_la_retraite' => $this->Date_de_la_retraite,
            'Direction' => $this->Direction,
            'Service' => $this->Service,
            'Email' => $this->Email,
            'Téléphone' => $this->Téléphone,
            'Matricule' => $this->Matricule,


        ];
    }
}
