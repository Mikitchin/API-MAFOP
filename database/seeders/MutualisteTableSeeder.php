<?php

namespace Database\Seeders;

use App\Models\Mutualiste;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MutualisteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Mutualiste::factory()->count(10)->create();
    }
}
