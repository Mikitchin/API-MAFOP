<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Mutualiste>
 */
class MutualisteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'Nom' => $this->faker->firstName(10, true),
            'Prenoms' => $this-> faker->lastName(50, true),
            'Date_de_Naissance' => $this->faker->date(),
            'Numéro_de_CNI' => $this->faker->sentence(),
            'Nom_du_père' => $this->faker->name(),
            'Nom_de_la_Mère' => $this->faker->name(),
            'Emploie' => $this->faker->sentence(1, true),
            'Grade' => $this->faker->sentence(1, true),
            'Date_de_première_prise_de_service' => $this->faker->date(),
            'Date_de_la_retraite' => $this->faker->date(),
            'Direction' => $this->faker->sentence(1, true),
            'Service' => $this->faker->sentence(1, true),
            'Email' => $this->faker->sentence(1, true),
            'Téléphone' => $this->faker->sentence(1, true),
            'Matricule' => $this->faker->sentence(1, true),

        ];
    }
}
