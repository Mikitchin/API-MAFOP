<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mutualistes', function (Blueprint $table) {
            $table->id();
            $table->string('Nom');
            $table->string('Prenoms');
            $table->date('Date_de_Naissance');
            $table->string('Numéro_de_CNI');
            $table->string('Nom_du_père');
            $table->string('Nom_de_la_Mère');
            $table->string('Emploie');
            $table->string('Grade');
            $table->date('Date_de_première_prise_de_service');
            $table->date('Date_de_la_retraite');
            $table->string('Direction');
            $table->string('Service');
            $table->string('Email');
            $table->string('Téléphone');
            $table->string('Matricule');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mutualistes');
    }
};
